const express = require('express');

const app = express();

var port = process.env.PORT || 4000;

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.status(200);
    res.send('Connection test');
    res.end();
});

// app.get('/help', (req, res) => {
//     res.send('help.html');
// });

app.listen(port, () => {
    console.log(`Server started on port ${port}`); 
});