document.getElementById('button').onclick = () => {

    var formData = new FormData();
    var files = document.getElementById('fileHadler');

    var fields = []

    for(var i = 0; i < 5; i++){
        var field = document.getElementById(`field-config${i}`);
        if(field.value !== ""){
            fields.push(field.value);        
        }
    };

    console.log('fields: ', fields);

    console.log("Files length: ", files.files.length);

    for(var i = 0; i < files.files.length; i++){
        formData.append(`test${i}`, files.files[i]);
    };

    for(var i = 0; i < fields.length; i++){
        if(fields[i] !== ""){
            formData.append(`file${i}`, fields[i]);
        }
    };

    // {"type": "graph", "format": "jpeg"}
    //heroku: https://converter-api.herokuapp.com/convertapi
    //localhost: http://localhost:3000/convertapi
    axios.post('https://converter-api.herokuapp.com/convertapi', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        responseType: "blob"
    })
        .then(function(response) {
            console.log('Response:', response);

            var contentType = Object.values(response.headers);

            console.log('Headers:', contentType);

            var blob = new Blob( [response.data], {type: contentType});
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = 'test.zip';
            link.click();
            
        })
        .catch(function(error) {
            console.log(error);
        });
};